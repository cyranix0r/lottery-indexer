#!/usr/bin/python3

__version__ = "0.1.0"
__prog__ = "lottoindex"
__authors__ = ["Thomas Robinson"]
__copyright__ = "GNU GPLv3 (https://choosealicense.com/licenses/gpl-3.0/)"

class Lottery:
    def __init__(self, Numbers, NumBalls):
        self.Numbers = Numbers
        self.NumBalls = NumBalls

    def Choose(self, n, k):
        """
            Modified n choose k function, which would normally be invoked with math module as:
            choose = lambda n, k: math.factorial(n) // math.factorial(k) // math.factorial(n - k)
            This version handles cases which would result in negative numbers and does the needful
        """
        if (n < 0) or (k < 0):
            print(f'Odd error, To: {n} Size: {k}') # Debug statement, this should *NEVER* happen in this class
        elif n < k:
            return 0    # Special case
        elif n == k:
            return 1
        else:
            if k < (n - k):
                delta = n - k
                iMax = k
            else:
                delta = k
                iMax = n - k

            ans = delta + 1

            for i in range(2, iMax + 1):
                ans = int((ans * (delta + i)) / i)

            return ans

    def toIndex(self, Balls):
        if len(Balls) != self.NumBalls:
            print(f'Number of balls ({len(Balls)} != expected ({self.NumBalls})')
            return -1
        ind = 0

        for i in range(0, self.NumBalls):
            d = (self.Numbers - 1) - (Balls[i] - 1)
            ind += self.Choose(d, self.NumBalls - i)

        ind = (self.Choose(self.Numbers, self.NumBalls) - 1) - ind
        return ind

    def fromIndex(self, Index):
        if Index < 0 or Index > (self.Choose(self.Numbers, self.NumBalls) - 1):
            print(f'Index {Index} not a valid input between 0 and {self.Choose(self.Numbers, self.NumBalls) - 1}')
            Index = 0
        ans = []
        a = self.Numbers
        b = self.NumBalls
        x = (self.Choose(self.Numbers, self.NumBalls) - 1) - Index

        for k in range(0, self.NumBalls):
            v = a - 1
            while self.Choose(v, b) > x:
                v -= 1
            ans += [v]
            x -= self.Choose(ans[k], b)
            a = ans[k]
            b -= 1
            ans[k] = (self.Numbers - 1) - ans[k]
            ans[k] += 1

        return ans